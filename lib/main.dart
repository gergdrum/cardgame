import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:card_game_test/player_model.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'card_model.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Card Game'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final databaseReference = FirebaseDatabase.instance.reference();
  final dbRef = FirebaseDatabase.instance.reference().child("Game");

  List<CardModel> cards = new List();
  List<CardModel> playerCards = new List();
  List<List<CardModel>> additionalCards = new List();
  List<CardModel> aces = new List();
  CardModel firstCard;
  CardModel secondCard;
  bool isStarted = false;
  bool isYourTurn = false;
  bool isFirst = true;
  CardModel previousCard;
  String cardPlace = '';
  String moreCardType = '';
  String winner = '';
  List<Player> players = new List();
  String playerName = "RománPlayer";
  int playerIndex;

  @override
  void initState() {
    super.initState();
    /*cards = createCards();
    createDeck();*/
    getLastIndex();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
    ]);
  }

  List<CardModel> createCards() {
    List<CardModel> result = new List();
    List<String> cardTypes = ['_of_diamonds', '_of_clubs', '_of_hearts', '_of_spades'];
    List<String> cardVersion = ['jack', 'ace', 'king', 'queen'];

    for (int j = 2; j < 11; j++) {
      for (var type in cardTypes) {
        result.add(CardModel('assets/card/' + j.toString() + type + '.png', j.toString(), type, j));
      }
    }
    for (var type in cardTypes) {
      for (var data in cardVersion) {
        if (data == 'ace') {
          aces.add(CardModel('assets/card/' + data + type + '.png', data, type, 14));
        } else if (data == 'king') {
          result.add(CardModel('assets/card/' + data + type + '.png', data, type, 13));
        } else if (data == 'queen') {
          result.add(CardModel('assets/card/' + data + type + '.png', data, type, 12));
        } else {
          result.add(CardModel('assets/card/' + data + type + '.png', data, type, 11));
        }
      }
    }
    return result;
  }

  void checkGameStarted() {
    const oneSec = const Duration(seconds: 6);
    new Timer.periodic(
        oneSec,
        (Timer t) => dbRef.once().then((DataSnapshot data) {
              if (data.value['players'].length == 2) {
                getPlayers();
                sleep(const Duration(seconds: 3));
                checkPlayerTurn();
                sleep(const Duration(seconds: 3));
                get();
                setState(() {
                  isStarted = true;
                  winner = '';
                });

                t.cancel();
              }
            }));
  }

  List<CardModel> createPlayerCards() {
    List<CardModel> result = new List();
    Random random = Random();
    int index = 0;
    for (var i = 0; i < 8; i++) {
      index = 1 + random.nextInt(this.cards.length - 1 - 1);
      CardModel card = this.cards[index];
      result.add(card);
      this.cards.removeAt(index);
    }
    print(result.length);
    return result;
  }

  void createPlayerDeck() {
    for (var player in players) {
      player.playerCards = createPlayerCards();
      print('LOL');
    }
  }

  createDeck() {
    Random random = Random();
    int firstIndex = 4 + random.nextInt(this.cards.length - 1 - 4);
    int secondIndex = 4 + random.nextInt(this.cards.length - 1 - 4);

    firstCard = this.cards[firstIndex];
    this.cards.removeAt(firstIndex);
    secondCard = this.cards[secondIndex];
    this.cards.removeAt(secondIndex);
  }

  checkPlayerTurn() {
    if (isStarted) {
      push();
    }

    const oneSec = const Duration(seconds: 5);
    new Timer.periodic(
        oneSec,
        (Timer t) => dbRef.once().then((DataSnapshot data) {
              if (data.value['players'][playerIndex]['isReady'] == 'true') {
                setState(() {
                  this.isYourTurn = true;
                  this.isFirst = true;
                  this.cardPlace = '';
                });
                if (isStarted) {
                  get();
                }
                t.cancel();
              }
            }));
  }

  void getLastIndex() {
    setState(() {
      dbRef.once().then((DataSnapshot data) {
        if (data.value == null) {
          playerIndex = 0;
        } else {
          playerIndex = data.value["players"].length;
        }
      });
    });
  }

  List<CardModel> getCards(int index) {
    List<CardModel> result = new List();
    dbRef.once().then((DataSnapshot data) {
      for (int i = 0; i < data.value["players"][index]['playerCards'].length; i++) {
        result.insert(
            i,
            CardModel(
                data.value["players"][index]["playerCards"][i]['imageUrl'],
                data.value["players"][index]["playerCards"][i]['value'],
                data.value["players"][index]["playerCards"][i]['type'],
                data.value["players"][index]["playerCards"][i]['numValue']));
      }
    });
    return result;
  }

  getPlayers() {
    dbRef.once().then((DataSnapshot data) {
      this.players.clear();
      for (int i = 0; i < data.value["players"].length; i++) {
        players.insert(i, Player(data.value["players"][i]["name"], data.value["players"][i]["isReady"], new List()));
      }
    });
    print(players.length);
    print('Player');
  }

  get() async {
    var test = dbRef.once().then((DataSnapshot data) async {
      setState(() {
        this.aces.clear();
        for (int i = 0; i < data.value["aces"].length; i++) {
          this.aces.add(CardModel(data.value["aces"][i]['imageUrl'], data.value["aces"][i]['value'],
              data.value["aces"][i]['type'], data.value["aces"][i]['numValue']));
        }

        this.cards.clear();
        for (int i = 0; i < data.value["cards"].length; i++) {
          this.cards.insert(
              i,
              CardModel(data.value["cards"][i]['imageUrl'], data.value["cards"][i]['value'],
                  data.value["cards"][i]['type'], data.value["cards"][i]['numValue']));
        }

        if (data.value["additionalCards"] != 'ures') {
          this.additionalCards.clear();
          for (int i = 0; i < data.value["additionalCards"].length; i++) {
            List<CardModel> list = new List();
            for (int j = 0; j < data.value["additionalCards"][i].length; j++) {
              list.insert(
                  j,
                  CardModel(
                      data.value["additionalCards"][i][j]['imageUrl'],
                      data.value["additionalCards"][i][j]['value'],
                      data.value["additionalCards"][i][j]['type'],
                      data.value["additionalCards"][i][j]['numValue']));
            }
            this.additionalCards.insert(i, list);
          }
        } else {
          this.additionalCards.clear();
        }
        this.firstCard = CardModel(data.value["firstCard"]['imageUrl'], data.value["firstCard"]['value'],
            data.value['firstCard']['type'], data.value["firstCard"]['numValue']);
        this.secondCard = CardModel(data.value["secondCard"]['imageUrl'], data.value["secondCard"]['value'],
            data.value['secondCard']['type'], data.value["secondCard"]['numValue']);

        print('HAHA');

        print(data.value["players"].length);

        this.players.clear();
        for (int i = 0; i < data.value["players"].length; i++) {
          List<CardModel> testCards = new List();
          if (data.value["players"][i]['playerCards'] != "ures") {
            for (int j = 0; j < data.value["players"][i]['playerCards'].length; j++) {
              testCards.insert(
                  j,
                  CardModel(
                      data.value["players"][i]["playerCards"][j]['imageUrl'],
                      data.value["players"][i]["playerCards"][j]['value'],
                      data.value["players"][i]["playerCards"][j]['type'],
                      data.value["players"][i]["playerCards"][j]['numValue']));
            }

            if (data.value["players"][i]['name'] == playerName) {
              playerCards = testCards;
            }

            players.insert(i, Player(data.value["players"][i]["name"], data.value["players"][i]["isReady"], testCards));
          }
        }
      });
    });
  }

  push() {
    setState(() {
      //players[playerIndex].playerCards = this.playerCards;

      this.playerCards = players[playerIndex].playerCards;
    });

    dbRef.update({
      'aces': 'ures',
    });
    for (int i = 0; i < this.aces.length; i++) {
      dbRef.child('aces').child(i.toString()).set({
        'imageUrl': this.aces[i].imageUrl,
        'value': this.aces[i].value,
        'type': this.aces[i].type,
        'numValue': this.aces[i].numValue,
      });
    }

    dbRef.update({
      'cards': 'ures',
    });
    for (int i = 0; i < this.cards.length; i++) {
      dbRef.child('cards').child(i.toString()).set({
        'imageUrl': this.cards[i].imageUrl,
        'value': this.cards[i].value,
        'type': this.cards[i].type,
        'numValue': this.cards[i].numValue,
      });
    }

    dbRef.update({
      'players': 'ures',
    });

    dbRef.update({'additionalCards': 'ures'});

    for (int i = 0; i < this.additionalCards.length; i++) {
      for (int j = 0; j < this.additionalCards[i].length; j++) {
        dbRef.child('additionalCards').child(i.toString()).child(j.toString()).set({
          'imageUrl': this.additionalCards[i][j].imageUrl,
          'value': this.additionalCards[i][j].value,
          'type': this.additionalCards[i][j].type,
          'numValue': this.additionalCards[i][j].numValue,
        });
      }
    }

    dbRef.child('firstCard').set({
      'imageUrl': this.firstCard.imageUrl,
      'value': this.firstCard.value,
      'type': this.firstCard.type,
      'numValue': this.firstCard.numValue,
    });
    dbRef.child('secondCard').set({
      'imageUrl': this.secondCard.imageUrl,
      'value': this.secondCard.value,
      'type': this.secondCard.type,
      'numValue': this.secondCard.numValue,
    });

    for (int i = 0; i < players.length; i++) {
      dbRef.child('players').child(i.toString()).set({
        'name': players[i].name,
        'isReady': 'false',
        'playerCards': 'ures',
      });

      for (int j = 0; j < this.players[i].playerCards.length; j++) {
        dbRef.child('players').child(i.toString()).child('playerCards').child(j.toString()).set({
          'imageUrl': this.players[i].playerCards[j].imageUrl,
          'value': this.players[i].playerCards[j].value,
          'type': this.players[i].playerCards[j].type,
          'numValue': this.players[i].playerCards[j].numValue,
        });
      }
    }

    dbRef
        .child('players')
        .child((playerIndex == players.length - 1 ? '0' : (playerIndex + 1).toString()))
        .update({'isReady': 'true'});

    dbRef.child('players').child((playerIndex.toString())).update({'isReady': 'false'});
  }

  checkReadyForNextRound(String cardPlace, CardModel card) {
    setState(() {
      if (isFirst) {
        this.isFirst = false;
      }
    });

    if (this.cardPlace == '') {
      setState(() {
        this.cardPlace = cardPlace;
      });
    }

    if (this.cardPlace == cardPlace) {
      setState(() {
        this.previousCard = card;
      });

      bool isValid = true;

      for (var playerCard in this.playerCards) {
        if (playerCard.type == card.type &&
            ((playerCard.numValue - card.numValue) == 1) &&
            (moreCardType == '' || moreCardType == 'line') &&
            playerCard.value != 'ace') {
          setState(() {
            moreCardType = 'line';
          });
          isValid = false;
          break;
        } else if ((playerCard.numValue == card.numValue) &&
            (moreCardType == '' || moreCardType == 'same') &&
            card.numValue != 2) {
          isValid = false;
          setState(() {
            moreCardType = 'same';
          });
          break;
        }
      }
      if (isValid) {
        setState(() {
          this.cardPlace = '';
          this.moreCardType = '';
          this.isYourTurn = false;
        });
        checkPlayerTurn();
      }
    } else {
      setState(() {
        this.cardPlace = '';
        this.moreCardType = '';
        this.isYourTurn = false;
      });
      checkPlayerTurn();
    }
  }

  @override
  Widget build(BuildContext context) {
    Random random = new Random();

    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.play_circle_filled),
              onPressed: () {
                //dbRef.remove();

                dbRef
                    .child('players')
                    .child(playerIndex.toString())
                    .set({'name': playerName, 'isReady': 'true', 'playerCards': ''});
                checkGameStarted();
              },
            ),
            IconButton(
              icon: Icon(Icons.refresh),
              onPressed: () {
                getPlayers();
                sleep(const Duration(seconds: 3));
                checkPlayerTurn();
                sleep(const Duration(seconds: 3));
                get();
                setState(() {
                  isStarted = true;
                  winner = '';
                });
              },
            ),
            IconButton(
              icon: Icon(Icons.refresh),
              onPressed: () {
                getPlayers();
                sleep(const Duration(seconds: 3));
                print(players.length);
                get();
                print(players.length);
              },
            ),
          ],
        ),
        body: Container(
          color: Colors.blue,
          width: double.infinity,
          height: double.infinity,
          child: !isStarted
              ? (winner != '' ? Text(winner) : Container())
              : Stack(
                  children: <Widget>[
                    // Max Size
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        height: 100.0,
                        width: 550.0,
                        child: _mainDeck(context),
                      ),
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: Container(
                        padding: EdgeInsets.all(8),
                        height: 120.0,
                        width: 550.0,
                        child: _additionalDeck(context),
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        height: 120.0,
                        width: double.infinity,
                        child: _playerDeck(context),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topRight,
                      child: Container(
                        height: 150.0,
                        width: 250.0,
                        child: _playersList(context),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 90, 0, 0),
                      child: RaisedButton(
                        child: Text("Get new card"),
                        onPressed: !this.isYourTurn || !this.isFirst
                            ? null
                            : () {
                                setState(() {
                                  print(this.cards.length);
                                  var index = 1 + random.nextInt(this.cards.length - 1 - 1);
                                  this.playerCards.add(this.cards[index]);
                                  this.cards.remove(this.cards[index]);
                                  this.isYourTurn = false;
                                  checkPlayerTurn();
                                });
                              },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 200, 0, 0),
                      child: !isStarted ? Text('') : (isYourTurn ? Text("Your turn") : Text('Wait')),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 140, 0, 0),
                      child: RaisedButton(
                        child: Text("Next"),
                        onPressed: !this.isYourTurn || this.isFirst
                            ? null
                            : () {
                                setState(() {
                                  this.isYourTurn = false;
                                });
                                checkPlayerTurn();
                              },
                      ),
                    ),
                  ],
                ),
        ));
  }

  Widget _playerDeck(BuildContext context) {
    List<Widget> list = new List<Widget>();
    List<Widget> secondList = new List<Widget>();
    for (var card in this.playerCards) {
      if (list.length >= 10) {
        secondList.add(
          !this.isYourTurn
              ? Container(
                  width: 70,
                  height: 70,
                  decoration: BoxDecoration(
                    image: DecorationImage(image: AssetImage(card.imageUrl), fit: BoxFit.fill),
                    shape: BoxShape.rectangle,
                  ),
                )
              : Draggable(
                  data: card,
                  child: Container(
                    width: 70,
                    height: 70,
                    decoration: BoxDecoration(
                      image: DecorationImage(image: AssetImage(card.imageUrl), fit: BoxFit.fill),
                      shape: BoxShape.rectangle,
                    ),
                  ),
                  feedback: Container(
                    width: 70,
                    height: 70,
                    decoration: BoxDecoration(
                      image: DecorationImage(image: AssetImage(card.imageUrl), fit: BoxFit.fill),
                      shape: BoxShape.rectangle,
                    ),
                  ),
                  childWhenDragging: Container(),
                ),
        );
      } else {
        list.add(
          !this.isYourTurn
              ? Container(
                  width: 70,
                  height: 70,
                  decoration: BoxDecoration(
                    image: DecorationImage(image: AssetImage(card.imageUrl), fit: BoxFit.fill),
                    shape: BoxShape.rectangle,
                  ),
                )
              : Draggable(
                  data: card,
                  child: Container(
                    width: 70,
                    height: 70,
                    decoration: BoxDecoration(
                      image: DecorationImage(image: AssetImage(card.imageUrl), fit: BoxFit.fill),
                      shape: BoxShape.rectangle,
                    ),
                  ),
                  feedback: Container(
                    width: 70,
                    height: 70,
                    decoration: BoxDecoration(
                      image: DecorationImage(image: AssetImage(card.imageUrl), fit: BoxFit.fill),
                      shape: BoxShape.rectangle,
                    ),
                  ),
                  childWhenDragging: Container(),
                ),
        );
      }
    }
    return Column(
      children: <Widget>[
        new Row(mainAxisAlignment: MainAxisAlignment.center, children: (list)),
        new Row(mainAxisAlignment: MainAxisAlignment.center, children: (secondList)),
      ],
    );
  }

  Widget _mainDeck(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: DragTarget(builder: (context, List<CardModel> candidateData, rejectedData) {
            return Container(
              width: 70,
              height: 70,
              decoration: BoxDecoration(
                image: DecorationImage(image: AssetImage(firstCard.imageUrl), fit: BoxFit.fill),
                shape: BoxShape.rectangle,
              ),
            );
          }, onWillAccept: (data) {
            print(data.value);
            print(data.type);
            print((this.cardPlace == '' || this.cardPlace == 'firstCard'));

            return true;
          }, onAccept: (data) {
            if (cardPlace == '' || cardPlace == 'firstCard') {
              bool value = false;

              if (moreCardType == '') {
                for (int k = 0; k < this.additionalCards.length; k++) {
                  if (this.additionalCards[k][this.additionalCards[k].length - 1].type == data.type ||
                      this.additionalCards[k][this.additionalCards[k].length - 1].value == data.value) {
                    value = true;
                  }
                }
              }

              if ((data.type == firstCard.type || data.value == firstCard.value) &&
                  data.value != '2' &&
                  !value &&
                  data.value != 'ace' &&
                  (this.cardPlace == '' || this.cardPlace == 'firstCard')) {
                print('lol');
                if (data.value == 'jack') {
                  setState(() {
                    this.playerCards.remove(data);
                  });

                  if (this.aces.length == 0) {
                    setState(() {
                      this.firstCard = data;
                    });

                    checkReadyForNextRound('firstCard', data);
                  } else {
                    setState(() {
                      this.firstCard = data;
                    });
                    showDialog(
                      barrierDismissible: false,
                      context: context,
                      builder: (BuildContext context) => _buildAceDialog(context, 'firstCard', data),
                    );
                  }
                } else if (data.value == 'queen' && data.type == firstCard.type) {
                  setState(() {
                    this.firstCard = data;
                  });

                  showDialog(
                    barrierDismissible: false,
                    context: context,
                    builder: (BuildContext context) => _buildQueenDialog(context, data, 'firstCard'),
                  );
                }

                if (data.value != 'jack') {
                  setState(() {
                    this.playerCards.remove(data);
                  });
                  setState(() {
                    this.firstCard = data;
                  });
                  print(this.playerCards.length);
                  checkReadyForNextRound('firstCard', data);
                }
              }
            }
          }),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: DragTarget(builder: (context, List<CardModel> candidateData, rejectedData) {
            return Container(
              width: 70,
              height: 70,
              decoration: BoxDecoration(
                image: DecorationImage(image: AssetImage(secondCard.imageUrl), fit: BoxFit.fill),
                shape: BoxShape.rectangle,
              ),
            );
          }, onWillAccept: (data) {
            print(data);
            print(this.secondCard);
            return true;
          }, onAccept: (data) {
            if (cardPlace == '' || cardPlace == 'secondCard') {
              bool value = false;
              if (moreCardType == '') {
                for (int k = 0; k < this.additionalCards.length; k++) {
                  if (this.additionalCards[k][this.additionalCards[k].length - 1].type == data.type ||
                      this.additionalCards[k][this.additionalCards[k].length - 1].value == data.value) {
                    value = true;
                  }
                }
              }

              if ((data.type == secondCard.type || data.value == secondCard.value) &&
                  data.value != '2' &&
                  !value &&
                  data.value != 'ace') {
                if (data.value == 'jack') {
                  setState(() {
                    this.playerCards.remove(data);
                  });
                  if (this.aces.length == 0) {
                    setState(() {
                      this.secondCard = data;
                    });
                    checkReadyForNextRound('secondCard', data);
                  } else {
                    setState(() {
                      this.secondCard = data;
                    });
                    showDialog(
                      barrierDismissible: false,
                      context: context,
                      builder: (BuildContext context) => _buildAceDialog(context, 'secondCard', data),
                    );
                  }
                } else if (data.value == 'queen' && data.type == secondCard.type) {
                  setState(() {
                    this.secondCard = data;
                  });

                  showDialog(
                    barrierDismissible: false,
                    context: context,
                    builder: (BuildContext context) => _buildQueenDialog(context, data, 'secondCard'),
                  );
                }

                if (data.value != 'jack' && data.value != 'queen') {
                  setState(() {
                    this.playerCards.remove(data);
                  });
                  setState(() {
                    this.secondCard = data;
                  });
                  checkReadyForNextRound('secondCard', data);
                }
              }
            }
          }),
        ),
      ],
    );
  }

  Widget _additionalDeck(BuildContext context) {
    List<Widget> list = new List<Widget>();
    // this.additionalCards.removeAt(0);

    if (this.additionalCards.length != 0) {
      print('IF');
      for (int i = 0; i < this.additionalCards.length; i++) {
        CardModel card = this.additionalCards[i][this.additionalCards[i].length - 1];
        list.add(
          Column(
            children: <Widget>[
              DragTarget(builder: (context, List<CardModel> candidateData, rejectedData) {
                return Container(
                  width: 70,
                  height: 70,
                  decoration: BoxDecoration(
                    image: DecorationImage(image: AssetImage(card.imageUrl), fit: BoxFit.fill),
                    shape: BoxShape.rectangle,
                  ),
                );
              }, onWillAccept: (data) {
                return true;
              }, onAccept: (data) {
                if (cardPlace == '' || cardPlace == 'additionalCard' + i.toString()) {
                  if (data.value != '2') {
                    if (data.type == card.type || data.value == card.value) {
                      card = data;
                      if (data.value == 'jack') {
                        setState(() {
                          this.playerCards.remove(data);
                        });
                        if (this.aces.length == 0) {
                          setState(() {
                            this.additionalCards[i].add(data);
                          });

                          checkReadyForNextRound('additionalCard' + i.toString(), data);
                        } else {
                          setState(() {
                            this.additionalCards[i].add(data);
                          });
                          showDialog(
                            barrierDismissible: false,
                            context: context,
                            builder: (BuildContext context) =>
                                _buildAceDialog(context, 'additionalCard' + i.toString(), data),
                          );
                        }
                      } else if (data.value == 'ace' && data.type == card.type) {
                        print('ACE');
                        showDialog(
                          barrierDismissible: false,
                          context: context,
                          builder: (BuildContext context) => _buildPlayerListDialog(context, 'ace', data, i),
                        );
                      } else if (data.value == 'queen' && data.type == card.type) {
                        showDialog(
                          barrierDismissible: false,
                          context: context,
                          builder: (BuildContext context) =>
                              _buildQueenDialog(context, data, 'additionalCard' + i.toString()),
                        );
                      }
                      if (data.value != 'ace') {
                        setState(() {
                          this.additionalCards[i].add(data);
                          this.playerCards.remove(data);
                        });

                        print('HAHA');
                        checkReadyForNextRound('additionalCard' + i.toString(), data);
                      }
                    }
                  } else {
                    setState(() {
                      this.additionalCards.add([data]);
                      this.playerCards.remove(data);
                    });

                    checkReadyForNextRound('additionalCard' + i.toString(), data);
                  }
                }
              }),
              Text(this.additionalCards[i].length.toString() + 'db'),
            ],
          ),
        );
      }
      return new Row(mainAxisAlignment: MainAxisAlignment.center, children: list);
    } else {
      return DragTarget(builder: (context, List<CardModel> candidateData, rejectedData) {
        return Container(
          width: 70,
          height: 70,
        );
      }, onWillAccept: (data) {
        return true;
      }, onAccept: (data) {
        if (data.value == '2') {
          setState(() {
            this.additionalCards.add([data]);
            this.playerCards.remove(data);
          });
          checkReadyForNextRound('additionalCard' + (this.additionalCards.length + 1).toString(), data);
        }
      });
    }
  }

  Widget _playersList(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      itemBuilder: (BuildContext context, int index) {
        return index != playerIndex
            ? ListTile(
                title: Text(
                  this.players[index].name,
                ),
                subtitle: Text(this.players[index].playerCards.length.toString() + 'db'),
              )
            : Container();
      },
      itemCount: players.length,
    );
  }

  Widget _buildQueenDialog(BuildContext context, CardModel card, String cardPlace) {
    return new AlertDialog(
      content: Text('Akarsz cserélni?'),
      actions: <Widget>[
        new FlatButton(
          child: Text('Igen'),
          onPressed: () {
            Navigator.of(context).pop();
            showDialog(
              barrierDismissible: false,
              context: context,
              builder: (BuildContext context) => _buildPlayerListDialog(context, 'queen', card, 0),
            );
          },
          textColor: Colors.green,
        ),
        new FlatButton(
          child: Text('Nem'),
          onPressed: () {
            setState(() {
              this.playerCards.remove(card);
            });
            checkReadyForNextRound(cardPlace, card);
            Navigator.of(context).pop();
          },
          textColor: Colors.red,
        ),
      ],
    );
  }

  Widget _buildPlayerListDialog(
    BuildContext context,
    String type,
    CardModel card,
    int additionalIndex,
  ) {
    List<Widget> list = new List<Widget>();
    for (int i = 0; i < players.length; i++) {
      i != playerIndex
          ? list.add(FlatButton(
              onPressed: () {
                if (type == 'queen') {
                  List<CardModel> list = new List();
                  this.playerCards.remove(card);
                  list = this.playerCards;
                  this.playerCards = players[i].playerCards;
                  players[i].playerCards = list;
                  this.isYourTurn = false;
                  checkPlayerTurn();
                  Navigator.of(context).pop();
                } else {
                  this.playerCards.remove(card);
                  this.aces.add(card);
                  for (int j = 0; j < this.additionalCards[additionalIndex].length; j++) {
                    players[i].playerCards.add(this.additionalCards[additionalIndex][j]);
                  }
                  this.additionalCards.removeAt(additionalIndex);
                  checkPlayerTurn();
                  Navigator.of(context).pop();
                }
              },
              child: Text(players[i].name)))
          : null;
    }

    return new AlertDialog(
      title: Text("Kit akarsz beszopatni?"),
      content: new Row(mainAxisAlignment: MainAxisAlignment.center, children: list),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          textColor: Theme.of(context).primaryColor,
        ),
      ],
    );
  }

  Widget _buildAceDialog(BuildContext context, String cardPlace, CardModel card) {
    List<Widget> list = new List<Widget>();

    for (var ace in this.aces) {
      list.add(
        Column(
          children: <Widget>[
            InkWell(
              onTap: () {
                setState(() {
                  this.playerCards.add(ace);
                  this.aces.remove(ace);
                });
                dbRef.update({
                  'aces': 'ures',
                });
                for (int i = 0; i < this.aces.length; i++) {
                  dbRef.child('aces').child(i.toString()).set({
                    'imageUrl': this.aces[i].imageUrl,
                    'value': this.aces[i].value,
                    'type': this.aces[i].type,
                    'numValue': this.aces[i].numValue,
                  });
                }
                checkReadyForNextRound(cardPlace, card);

                Navigator.pop(context);
              },
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  width: 70,
                  height: 70,
                  decoration: BoxDecoration(
                    image: DecorationImage(image: AssetImage(ace.imageUrl), fit: BoxFit.fill),
                    shape: BoxShape.rectangle,
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }

    return new AlertDialog(
      title: const Text('Ace dialog'),
      content: new Row(mainAxisAlignment: MainAxisAlignment.center, children: list),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          textColor: Theme.of(context).primaryColor,
        ),
      ],
    );
  }
}
