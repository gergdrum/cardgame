import 'card_model.dart';

class Player {
  String name;
  String isReady;
  List<CardModel> playerCards;

  Player(this.name, this.isReady, this.playerCards);
}
