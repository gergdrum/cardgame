class CardModel {
  String imageUrl;
  String value;
  String type;
  int numValue;

  CardModel(this.imageUrl, this.value, this.type, this.numValue);
}
